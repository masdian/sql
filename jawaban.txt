1. Membuat database "myshop"

    CREATE DATABASE myshop;


2. Membuat tabel 'users', 'items', dan 'categories'

    CREATE TABLE users ( 
        id INT AUTO_INCREMENT PRIMARY KEY, 
        name VARCHAR(255), 
        email VARCHAR(255), 
        password VARCHAR(255));

    CREATE TABLE items ( 
        id INT AUTO_INCREMENT PRIMARY KEY, 
        name VARCHAR(255), 
        description VARCHAR(255), 
        price INT, 
        stock INT, 
        category_id INT, 
        FOREIGN KEY (category_id) REFERENCES categories(id));  

    CREATE TABLE categories ( 
        id INT AUTO_INCREMENT PRIMARY KEY, 
        name VARCHAR(255)); 


3. Mengisi tabel 'users', 'categories', dan 'items'

    INSERT INTO users (name, email, password) VALUES 
        ('John Doe', 'john@doe.com', 'john123'),
        ('Jane Doe', 'jane@doe.com', 'jenita123');

    INSERT INTO categories (name) VALUES
        ('gadget'),
        ('cloth'),
        ('men'),
        ('women'),
        ('branded');

    INSERT INTO items (name, description, price, stock, category_id) VALUES 
        ('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1),
        ('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
        ('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);


4. Mengambil Data dari Database

4.a Mengambil data users

        SELECT id, name, email FROM user;

4.b Mengambil data items

        SELECT * FROM items WHERE price > 1000000;

        SELECT * FROM items WHERE name LIKE '%sang%';

4.c Menampilkan data items join dengan kategori

        SELECT items.*, categories.name AS kategori FROM 
            items LEFT JOIN categories ON items.id = categories.id;


5. Mengubah Data dari Database

    UPDATE items SET price = 2500000 WHERE name LIKE('Sumsang%');

    



